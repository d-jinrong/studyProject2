# -*- coding: utf-8 -*-
# @Author : jinrong


#定义animal类
class Aniaml:
    #定义构造函数
    def __init__(self,name,color,age,gender):
        self.name = name
        self.color = color
        self.age = age
        self.gender = gender
    #定义方法：动物会叫
    def animal_calls(self):
        print("动物会叫")
    # 定义方法：动物会跑
    def animal_run(self):
        print("动物会跑")
#新建猫类，继承动物类，并添加一个hair属性
class Cat(Aniaml):
    def __init__(self, name, color, age, gender,hair):
        super().__init__( name, color, age, gender)
        self.hair = hair

    #新添加一个捉老鼠方法
    def catch_mouse(self):
        print("捉到了老鼠")
    #重写会叫方法，改成喵喵叫
    def animal_calls(self):
        print("喵喵喵")

#创建子类狗
class Dog(Aniaml):
    def __init__(self,name, color, age, gender,hair):
        super().__init__(name, color, age, gender)
        self.hair = hair

    #新添加一个会看家方法
    def watch_home(self):
        print("狗狗会看家")
    # 重写会叫方法，改成汪汪叫
    def animal_calls(self):
        print("汪汪汪")

#在入口函数中创建类的实例
if __name__ == '__main__':
    cat1=Cat
    print("小花","黑色",3,"雌性","short hair")
    cat1.catch_mouse()

    dog1=Dog
    print("小白","白色",2,"雄性","long hair")
    dog1.watch_home()







